package br.com.jonasmferreira.testeneon.adapter;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.jonasmferreira.testeneon.R;
import br.com.jonasmferreira.testeneon.domain.Contato;
import br.com.jonasmferreira.testeneon.utils.MyApplication;
import br.com.jonasmferreira.testeneon.utils.Tools;

/**
 * Created by Flavia Galante on 27/08/2017.
 */

public class ContatoAdapter extends RecyclerView.Adapter<ContatoAdapter.ViewHolder>{
    List<Contato> contatoList = new ArrayList<>();
    Context context;
    public ContatoAdapter(List<Contato> contatoList) {
        this.contatoList = contatoList;
    }

    public ContatoAdapter(List<Contato> contatoList, Context context) {
        this.contatoList = contatoList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View repositoryView = inflater.inflate(R.layout.item_contato, parent, false);

        ViewHolder viewHolder = new ViewHolder(repositoryView);
        return viewHolder;
    }

    public Bitmap loadContactPhoto(ContentResolver cr, long  id) {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(cr, uri);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Contato contato = contatoList.get(position);

        TextView tvContatoNome = holder.tvContatoNome;
        tvContatoNome.setText(contato.nome);

        TextView tvContatoTelefone = holder.tvContatoTelefone;
        tvContatoTelefone.setText(Tools.formatPhoneNumber(contato.numero));


        TextView tvValor = holder.tvValor;

        Double d = new Double(contato.valor);

        if(d!=null){
            if(contato.valor > -1) {
                tvValor.setText(Tools.formatMoney(contato.valor));
                tvValor.setVisibility(View.VISIBLE);
            }
        }

        Bitmap photo;
        ImageView civFotoContato = holder.civFotoContato;
        try {
            Bitmap b = Tools.loadContactPhoto(context.getContentResolver(), Long.valueOf(contato.id));
            if(b!=null){
                civFotoContato.setImageBitmap(loadContactPhoto(context.getContentResolver(), Long.valueOf(contato.id)));
            }else{
                throw new NullPointerException("SEM IMAGEM");
            }

        }catch (Exception e){
            //e.printStackTrace();
            String initials = Tools.getInitials(contato.nome);
            initials = initials.length() > 1 ? initials.substring(0,2) : initials;
            TextDrawable drawable = TextDrawable.builder()
                    .beginConfig()
                    .width(120)  // width in px
                    .height(120) // height in px
                    .endConfig()
                    .buildRect(initials, Color.RED);

            civFotoContato.setImageDrawable(drawable);
        }
    }

    @Override
    public int getItemCount() {
        return contatoList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvContatoNome,tvContatoTelefone,tvValor;
        public ImageView civFotoContato;
        public ViewHolder(View itemView) {
            super(itemView);
            tvContatoNome = (TextView) itemView.findViewById(R.id.tvContatoNome);
            tvContatoTelefone = (TextView) itemView.findViewById(R.id.tvContatoTelefone);
            civFotoContato = (ImageView)itemView.findViewById(R.id.civFotoContato);
            tvValor = (TextView) itemView.findViewById(R.id.tvValor);
        }
    }
}

