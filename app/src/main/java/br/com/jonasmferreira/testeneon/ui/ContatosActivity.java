package br.com.jonasmferreira.testeneon.ui;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.jonasmferreira.testeneon.R;
import br.com.jonasmferreira.testeneon.adapter.ContatoAdapter;
import br.com.jonasmferreira.testeneon.domain.Contato;
import br.com.jonasmferreira.testeneon.utils.EndlessRecyclerViewScrollListener;
import br.com.jonasmferreira.testeneon.utils.NumberTextWatcher;
import br.com.jonasmferreira.testeneon.utils.RecyclerItemClickListener;
import br.com.jonasmferreira.testeneon.utils.SimpleDividerItemDecoration;
import br.com.jonasmferreira.testeneon.utils.Tools;


import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class ContatosActivity extends AppCompatActivity {
    public static String TAG = ContatosActivity.class.getSimpleName();
    List<Contato> contatoList = new ArrayList<>();
    Dialog progressDialog;
    Context context;
    private android.support.v7.widget.RecyclerView rvContatos;
    private EndlessRecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    ContatoAdapter adapter;
    String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contatos);
        context = this;
        adapter = new ContatoAdapter(contatoList,context);
        loadComponents();
        loadActions();
        loadData();
    }

    protected void loadComponents(){
        this.rvContatos = (RecyclerView) findViewById(R.id.rvContatos);
        linearLayoutManager = new LinearLayoutManager(this);
        rvContatos.setLayoutManager(linearLayoutManager);
        rvContatos.addItemDecoration(new SimpleDividerItemDecoration(this));
    }

    protected void loadActions(){
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //SearchController.getRepository(String.valueOf(page));
            }
        };
        rvContatos.addOnScrollListener(scrollListener);
        rvContatos.addOnItemTouchListener(
                new RecyclerItemClickListener(context, rvContatos ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        LayoutInflater inflater = LayoutInflater.from(ContatosActivity.this);


                        final Contato contato = contatoList.get(position);


                        View subView = inflater.inflate(R.layout.form_transf, null);

                        final EditText subEditText = (EditText)subView.findViewById(R.id.dialogEditText);
                        subEditText.addTextChangedListener(new NumberTextWatcher(subEditText,new Locale( "pt", "BR" )));
                        final TextView tvContatoNome = (TextView)subView.findViewById(R.id.tvContatoNome);
                        final TextView tvContatoNumero = (TextView)subView.findViewById(R.id.tvContatoNumero);
                        final ImageView subImageView = (ImageView)subView.findViewById(R.id.civImageContact);
                        final Button btSendTransf = (Button)subView.findViewById(R.id.btSendTransf);

                        tvContatoNome.setText(contato.nome);
                        tvContatoNumero.setText(Tools.formatPhoneNumber(contato.numero));

                        try {
                            Bitmap b = Tools.loadContactPhoto(context.getContentResolver(), Long.valueOf(contato.id));
                            if(b!=null){
                                subImageView.setImageBitmap(b);
                            }else{
                                throw new NullPointerException("SEM IMAGEM");
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                            String initials = Tools.getInitials(contato.nome);
                            initials = initials.length() > 1 ? initials.substring(0,2) : initials;
                            TextDrawable drawable = TextDrawable.builder()
                                    .beginConfig()
                                    .width(120)  // width in px
                                    .height(120) // height in px
                                    .endConfig()
                                    .buildRect(initials, Color.RED);

                            subImageView.setImageDrawable(drawable);
                        }

                        AlertDialog.Builder builder = new AlertDialog.Builder(ContatosActivity.this);

                        builder.setView(subView);
                        AlertDialog alertDialog = builder.create();
                        final AlertDialog dialog = builder.show();
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        btSendTransf.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String brl = subEditText.getText().toString();
                                brl = brl.replaceAll("R\\$", "");
                                brl = brl.replaceAll("\\.", "");
                                brl = brl.replaceAll(",",".");
                                Log.d(TAG,"Text: "+brl);

                                sendMoney(token,String.valueOf(contato.id),brl);

                                dialog.dismiss();
                            }
                        });

                    }
                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );
    }

    protected void sendMoney(String token, String id, String valor){
        showProgressDialog("Enviando tranferência","Processo iniciado!");
        final RequestParams params = new RequestParams();
        params.put("ClienteId",id);
        params.put("token",token);
        params.put("valor",valor);
        AsyncHttpClient client = new AsyncHttpClient();
        String url = "http://processoseletivoneon.azurewebsites.net/SendMoney";
        Log.d(TAG,"PARAMS: "+params.toString());
        client.post(url, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d(TAG,"Errors: "+responseString.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                        showErrors("OPS","Não foi possivel enviar a transferência");
                    }
                });

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d(TAG, "Resposta: "+responseString);

                if(responseString.equals("true")) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            closeProgressBar();
                            showSuccess("BELEZA!", "Transferencia enviada com sucesso");
                        }
                    });
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            closeProgressBar();
                            showErrors("OPS","Não foi possivel enviar a transferência", false);
                        }
                    });
                }
            }
        });
    }
    protected void loadData(){
        rvContatos.setAdapter(adapter);
        this.rvContatos.setVisibility(View.INVISIBLE);
        showProgressDialog("Buscando contatos");
        new Thread(new Runnable() {
            @Override
            public void run() {
                getContacts();
            }
        }).start();
        generateToken();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    protected void generateToken(){
        final RequestParams params = new RequestParams();
        params.put("nome",getIntent().getStringExtra("nome"));
        params.put("email",getIntent().getStringExtra("email"));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setUserAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        String url = "http://processoseletivoneon.azurewebsites.net/GenerateToken";
        client.get(url, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                closeProgressBar();
                                showErrors("OPS","Não foi possivel buscar o token, tente mais tarde", true);
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Log.d(TAG, "Token: "+responseString);
                        token = responseString.replaceAll("\"","");

                    }
                }
        );
    }


    protected void getContacts(){
        contatoList.clear();
        Cursor contacts = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME+" ASC");
        int idFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup._ID);
        int nameFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
        int numberFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup.NUMBER);
        int imageFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI);

        while(contacts.moveToNext()) {
            String phoneNumber = contacts.getString(contacts
                    .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));


            if (phoneNumber.equals("1")) {
                Contato contato = new Contato();
                contato.id = contacts.getInt(idFieldColumnIndex);
                contato.nome = contacts.getString(nameFieldColumnIndex);
                Cursor phones = getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = " + contato.id, null, null);
                while (phones.moveToNext()) {
                    phoneNumber = phones
                            .getString(phones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                }
                phones.close();
                contato.numero = phoneNumber;
                if(contacts.getString(imageFieldColumnIndex)!=null)
                    contato.image = contacts.getString(imageFieldColumnIndex);

                contatoList.add(contato);
            }

        }
        contacts.close();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                closeProgressBar();
                rvContatos.setVisibility(View.VISIBLE);
                rvContatos.post(new Runnable() {
                    @Override
                    public void run() {
                        final int curSize = adapter.getItemCount();
                        adapter.notifyItemRangeInserted(curSize, contatoList.size() - 1);
                    }
                });
            }
        });

    }

    protected void showProgressDialog(String msg) {
        showProgressDialog("Aguarde", msg);
    }

    protected void showProgressDialog(String title, String msg) {
        progressDialog =  new AwesomeProgressDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogInfoBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .show();

    }

    protected void closeProgressBar() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    protected void showErrors(String msg){
        showErrors("Ops",msg);
    }
    protected void showErrors(String title,String msg){
        showErrors("Ops",msg,false);
    }
    protected void showErrors(String title,String msg, final boolean isClose){
        progressDialog = new AwesomeErrorDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                .setCancelable(true).setButtonText(getString(R.string.dialog_ok_button))
                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                .setButtonText(getString(R.string.dialog_ok_button))
                .setErrorButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        if(isClose) {
                            // click
                            closeProgressBar();
                            finish();
                            Tools.goTo(context,MainActivity.class,false);
                        }
                    }
                })
                .show();

    }
    protected void showSuccess(String msg){
        showSuccess("Ops",msg);
    }
    protected void showSuccess(String title,String msg){
        progressDialog = new AwesomeSuccessDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogSuccessBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_success, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText(getString(R.string.dialog_yes_button))
                .setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        //click
                    }
                })
                .show();

    }
}
