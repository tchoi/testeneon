package br.com.jonasmferreira.testeneon.ui;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeErrorDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeProgressDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.jonasmferreira.testeneon.R;
import br.com.jonasmferreira.testeneon.adapter.ContatoAdapter;
import br.com.jonasmferreira.testeneon.domain.Contato;
import br.com.jonasmferreira.testeneon.utils.EndlessRecyclerViewScrollListener;
import br.com.jonasmferreira.testeneon.utils.RecyclerItemClickListener;
import br.com.jonasmferreira.testeneon.utils.SimpleDividerItemDecoration;
import br.com.jonasmferreira.testeneon.utils.Tools;
import cz.msebera.android.httpclient.Header;


import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

public class HistoricoActivity extends AppCompatActivity {
    public static String TAG = HistoricoActivity.class.getSimpleName();
    List<Contato> contatoList = new ArrayList<>();
    Dialog progressDialog;
    Context context;
    private android.support.v7.widget.RecyclerView rvContatos;
    private EndlessRecyclerViewScrollListener scrollListener;
    LinearLayoutManager linearLayoutManager;
    ContatoAdapter adapter;
    String token = "";
    BarChart chart ;
    ArrayList<BarEntry> BARENTRY ;
    ArrayList<String> BarEntryLabels ;
    BarDataSet Bardataset ;
    BarData BARDATA ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico);
        context = this;
        adapter = new ContatoAdapter(contatoList,context);
        loadComponents();
        loadActions();
        loadData();

    }

    protected void loadComponents(){
        this.rvContatos = (RecyclerView) findViewById(R.id.rvContatos);
        chart = (BarChart) findViewById(R.id.chart);
        BARENTRY = new ArrayList<>();
        BarEntryLabels = new ArrayList<String>();

        linearLayoutManager = new LinearLayoutManager(this);
        rvContatos.setLayoutManager(linearLayoutManager);
        rvContatos.addItemDecoration(new SimpleDividerItemDecoration(this));
    }

    protected void loadData(){
        rvContatos.setAdapter(adapter);
        this.rvContatos.setVisibility(View.INVISIBLE);
        showProgressDialog("Buscando históricos");
        generateToken();

    }

    protected void loadActions(){
        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //SearchController.getRepository(String.valueOf(page));
            }
        };
        rvContatos.addOnScrollListener(scrollListener);
        rvContatos.addOnItemTouchListener(
                new RecyclerItemClickListener(context, rvContatos ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }
                    @Override
                    public void onLongItemClick(View view, int position) {
                    }
                })
        );
    }

    protected void getHistorico(){
        final RequestParams params = new RequestParams();
        params.put("token",token);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setUserAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        String url = "http://processoseletivoneon.azurewebsites.net/GetTransfers";
        client.get(url, params,new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // called when response HTTP status is "200 OK"
                Log.d(TAG,"HIST: "+response.toString());

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                // called when response HTTP status is "200 OK"
                Log.d(TAG,"HIST: "+response.toString());
                List<String> list = new ArrayList<String>();
                try{
                    for(int i=0; i<response.length();i++){
                        JSONObject o = response.getJSONObject(i);
                        list.add(o.getString("ClienteId"));

                    }
                    getContacts(StringUtils.join(list," ,"),response);

                }catch(Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                    }
                });

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray errorResponse) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                    }
                });
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        closeProgressBar();
                    }
                });
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });

    }

    protected void getContacts(String ids,JSONArray response){
        Log.d(TAG,"IDS: "+ids);
        contatoList.clear();
        ContentResolver cr = getContentResolver();
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String where = ContactsContract.Contacts._ID + " IN ("+ids+")";
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME;
        Cursor contacts = getContentResolver().query(
                uri,
                null,
                where,
                null, sortOrder);

        int idFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup._ID);
        int nameFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
        int numberFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup.NUMBER);
        int imageFieldColumnIndex = contacts.getColumnIndex(ContactsContract.PhoneLookup.PHOTO_URI);


        while(contacts.moveToNext()) {
            String phoneNumber = contacts.getString(contacts
                    .getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));


            if (phoneNumber.equals("1")) {
                Contato contato = new Contato();
                contato.id = contacts.getInt(idFieldColumnIndex);
                contato.nome = contacts.getString(nameFieldColumnIndex);
                Cursor phones = getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                        + " = " + contato.id, null, null);
                while (phones.moveToNext()) {
                    phoneNumber = phones
                            .getString(phones
                                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                }
                phones.close();
                contato.numero = phoneNumber;
                if(contacts.getString(imageFieldColumnIndex)!=null)
                    contato.image = contacts.getString(imageFieldColumnIndex);

                double soma = 0.0;
                try{
                    for(int i=0; i<response.length();i++){
                        JSONObject o = response.getJSONObject(i);
                        if(o.getString("ClienteId").equals(String.valueOf(contato.id))){
                            soma+= o.getDouble("Valor");
                        }
                    }

                }catch(Exception e){
                    e.printStackTrace();
                }
                contato.valor = soma;
                contatoList.add(contato);
            }
        }
        contacts.close();

        Log.d(TAG,"liST: "+contatoList.toString());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                closeProgressBar();

                for(int i=0; i<contatoList.size();i++){
                    Contato c = contatoList.get(i);
                    BARENTRY.add(new BarEntry(Float.parseFloat(String.valueOf(c.valor)), i));
                    BarEntryLabels.add(c.nome);
                }

                Bardataset = new BarDataSet(BARENTRY, "Tranferências em R$");
                BARDATA = new BarData(BarEntryLabels, Bardataset);
                Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);


                chart.setData(BARDATA);
                chart.animateY(1000);

                chart.setDescription("");

                rvContatos.setVisibility(View.VISIBLE);
                rvContatos.post(new Runnable() {
                    @Override
                    public void run() {
                        final int curSize = adapter.getItemCount();
                        adapter.notifyItemRangeInserted(0, contatoList.size());
                    }
                });
            }
        });

    }

    protected void generateToken(){
        final RequestParams params = new RequestParams();
        params.put("nome",getIntent().getStringExtra("nome"));
        params.put("email",getIntent().getStringExtra("email"));
        AsyncHttpClient client = new AsyncHttpClient();
        client.setUserAgent("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        String url = "http://processoseletivoneon.azurewebsites.net/GenerateToken";
        client.get(url, params, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                closeProgressBar();
                                showErrors("OPS","Não foi possivel buscar o token, tente mais tarde", true);
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        token = responseString.replaceAll("\"","");

                        Log.d(TAG, "Token: "+token);

                        getHistorico();

                    }
                }
        );
    }

    protected void showProgressDialog(String msg) {
        showProgressDialog("Aguarde", msg);
    }

    protected void showProgressDialog(String title, String msg) {
        progressDialog =  new AwesomeProgressDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogInfoBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .show();

    }

    protected void closeProgressBar() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.cancel();
    }

    protected void showErrors(String msg){
        showErrors("Ops",msg);
    }
    protected void showErrors(String title,String msg){
        showErrors("Ops",msg,false);
    }
    protected void showErrors(String title,String msg, final boolean isClose){
        progressDialog = new AwesomeErrorDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogErrorBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                .setCancelable(true).setButtonText(getString(R.string.dialog_ok_button))
                .setButtonBackgroundColor(R.color.dialogErrorBackgroundColor)
                .setButtonText(getString(R.string.dialog_ok_button))
                .setErrorButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        if(isClose) {
                            // click
                            closeProgressBar();
                            finish();
                            Tools.goTo(context,MainActivity.class,false);
                        }
                    }
                })
                .show();

    }
    protected void showSuccess(String msg){
        showSuccess("Ops",msg);
    }
    protected void showSuccess(String title,String msg){
        progressDialog = new AwesomeSuccessDialog(this)
                .setTitle(title)
                .setMessage(msg)
                .setColoredCircle(R.color.dialogSuccessBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_success, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText(getString(R.string.dialog_yes_button))
                .setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        //click
                    }
                })
                .show();

    }
}
