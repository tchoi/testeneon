package br.com.jonasmferreira.testeneon.domain;

import android.net.Uri;

/**
 * Created by Flavia Galante on 26/08/2017.
 */

public class Contato {
    public int id;
    public String nome;
    public String numero;
    public String image;
    public double valor = -1;

    public Contato() {
    }

    public Contato(int id, String nome, String numero, String image, double valor) {
        this.id = id;
        this.nome = nome;
        this.numero = numero;
        this.image = image;
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Contato{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", numero='" + numero + '\'' +
                ", image='" + image + '\'' +
                ", valor='" + valor + '\'' +
                '}';
    }
}
