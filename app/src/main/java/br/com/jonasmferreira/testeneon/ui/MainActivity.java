package br.com.jonasmferreira.testeneon.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.jonasmferreira.testeneon.R;
import br.com.jonasmferreira.testeneon.utils.Tools;
import permission.auron.com.marshmallowpermissionhelper.ActivityManagePermission;
import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import permission.auron.com.marshmallowpermissionhelper.PermissionUtils;

public class MainActivity extends ActivityManagePermission {

    TextView tvNome,tvEmail;
    ImageView ciProfileImage;
    Button btSendMoney, btSendHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadComponents();
        loadActions();
        loadPermissions();
    }

    protected void loadComponents(){
        tvNome = (TextView)findViewById(R.id.tvNome);
        tvEmail = (TextView)findViewById(R.id.tvEmail);
        ciProfileImage = (ImageView)findViewById(R.id.ciProfileImage);
        btSendMoney = (Button)findViewById(R.id.btSendMoney);
        btSendHistory = (Button)findViewById(R.id.btSendHistory);
    }

    protected void loadActions(){
        btSendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tools.goTo(MainActivity.this,ContatosActivity.class,false);
                Intent it = new Intent(MainActivity.this,ContatosActivity.class);
                it.putExtra("nome",tvNome.getText().toString());
                it.putExtra("email",tvEmail.getText().toString());
                startActivity(it);
            }
        });
        btSendHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(MainActivity.this,HistoricoActivity.class);
                it.putExtra("nome",tvNome.getText().toString());
                it.putExtra("email",tvEmail.getText().toString());
                startActivity(it);
            }
        });
    }

    protected void loadPermissions(){
        askCompactPermissions(new String[]{
                PermissionUtils.Manifest_READ_CONTACTS
        }, new PermissionResult() {
            @Override
            public void permissionGranted() {
                //permission granted
                //replace with your action
            }

            @Override
            public void permissionDenied() {
                //permission denied
                //replace with your action
            }
            @Override
            public void permissionForeverDenied() {
                // user has check 'never ask again'
                // you need to open setting manually
                //  Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                //  Uri uri = Uri.fromParts("package", getPackageName(), null);
                //   intent.setData(uri);
                //  startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
            }
        });
    }
}
